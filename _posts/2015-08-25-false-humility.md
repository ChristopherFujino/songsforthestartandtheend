---
layout: post
title:  "The Hidden Fear Behind False Humility"
date:   2015-08-25 09:41:00
categories: spiritual
---

What does biblical humility look like? Let's take a page from the book of the most humble man on the face of the earth:

> Now the man Moses was very humble, more than any man who was on the face of the earth - Moses (Numbers 12:3 NASB)

Though divinely inspired by God, we have Moses writing the book Numbers and explaining to future generations of God's people how he was the most humble man...on the planet! If this offends our sensibility of what humility looks like, then perhaps we are the ones with a distorted perception of humility, rather than Moses.

If we look at the context of this verse, Miriam and Aaron, Moses' siblings, have just accused Moses of impropriety over marrying a Cushite woman. Also, by saying to themselves, "has the LORD indeed spoken only through Moses?", there is a suggestion that Moses has grown proud, presuming that only he hears God's voice. Moses, in a demonstration of this great humility, neither capitulates under the pressure of his siblings nor rebukes them in his own defense, but allows God to be his defense.

And yet, clearly Moses has a healthy self-awareness, recognizes that he is the most humble man on the face of the earth, and isn't afraid to record it for all posterity. He also does not appear threatened by the accusations of his siblings. It's interesting to note that scripture never says whether or not there was any validity to the content of Miriam and Aaron's accusations, only that the heart behind it was displeasing to God.

From Moses, we get a portrait of what God wants our humility to look like. If our perception of humility differs from this biblical picture, we may suffer from *false humility*. The following diagram shows some of the symptoms of false humility, along with the corresponding healthy attitude God wants his children to possess.

False Humility | God's Heart for Me
---|---
I'm afraid people will think I'm proud | I am secure in my own heart, even if I'm judged by others (Psalm 25:2)
I should think less of myself than others do | I will believe God's best about myself, even when others think less of me (Psalm 23:5)
I don't want anyone to see my pride | I want any pride that I have to come to the surface so I can overcome it (Psalm 139:23-24, Proverbs 27:6)
I am not special, I will not do great things, I am just an ordinary person | I am awesome, I have a great future, and I want to do great things with God (Jeremiah 29:11)

False humility is rooted in fear and insecurity. We use it to protect ourselves from being embarassed or shamed. False humility lowers our self-esteem, and keeps us from aspiring to be greater. When we live in false humility, we are really partnering with the accuser of the brethren, who accuses us before God day and night (Revelation 12:10). Stop believing the accuser, and start believing the savior.

God thinks highly of you. He believed in you before anyone else did. He has seen every mistake you've ever made, and still has great plans for your life. When you pursue a calling and destiny that is greater than yourself, you will grow in true humility, and the LORD will lift you up.
