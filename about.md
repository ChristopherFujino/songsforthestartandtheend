---
layout: page
title: About
permalink: /about/
---

> You love to hear the story, again and again how it all got started way back when - MC Shan

> All my powers of expression, I thought so sublime could never do you justice in reason or rhyme - Bob Dylan

This is my story, this is my song. 

It is hosted on [github](http://github.com) using [Jekyll](http://jekyllrb.com).
